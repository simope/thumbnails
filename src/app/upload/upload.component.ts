import { Router } from '@angular/router';
import { MediaService } from './../services/media.service';
import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  constructor(private mediaService:MediaService, private router:Router) { }

  ngOnInit() {
  }

  submitForm = (event:any, data:any) => {
    console.log(event);
    console.log(data);
    const file = event.target.querySelector('input[type=file]').files[0];

    const fd = new FormData();
    fd.append('file',file);
    fd.append('title',data.title);
    fd.append('description',data.description);

    this.mediaService.upload(fd).subscribe
      (
        data => {
          console.log(data);
        }
      )
  }

}
