import { LoginService } from './login.service';
import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class MediaService {

  private url: string = 'http://media.mw.metropolia.fi/wbma';

  constructor(private http: Http, private loginService:LoginService) {
  }

  getMedia = () => {
    return this.http.get(this.url + '/media')
      .map(
        res =>
          res.json()
      );
  }

  getNew = () => {
    let start:number = 0;
    let limit:number = 10;
    return this.http.get(this.url + '/media?start='+start+'&limit='+limit)
      .map(
        res =>
          res.json()
      );
  }

  upload = (fData:any) => {
    return this.http.post(this.url + '/media?token='+this.loginService.getUser().token, fData).map(
      res => res.json()
    );
  }

}
